import React, { PureComponent } from 'react'
import style from './App.module.css'
// import Person from '../components/Persons/Person/Person'
import Persons from '../components/Persons/Persons'
import Cockpit from '../components/Cockpit/Cockpit'
// import ErrorBoundry from './ErrorBoundary/ErrorBoundary'
import Ax from '../hoc/Ax';
import withClass from '../hoc/withClass';

export const AuthContext = React.createContext(false);

class App extends PureComponent {

  constructor(props) {
    super(props);
    // console.log('[app.js] inside constructor', props);
    this.state = {
      persons : [
        {id: 'qwe', name: 'Max', age: 28},
        {id: 'asd', name: 'Manu', age: 29},
        {id: 'zxc', name: 'Stephanie', age: 26}
      ],
      otherState: 'some other value',
      showPersons: false, 
      toggleClicked: 0,
      authenticated: false
    }
  }

  componentWillMount() {
    // console.log('[app.js] inside componentWillMount()')
  }
  
  componentDidMount() {
      // console.log('[app.js] inside componentDidMount()')
  }

  // shouldComponentUpdate(nextProps, nextState) {
  //   console.log('[UPDATE APP.js] Inside shouldComponentUpdate', nextProps, nextState);
  //   return nextState.persons !== this.state.persons ||
  //     nextState.showPersons !== this.state.showPersons;
  // }

  componentWillUpdate(nextProps, nextState) {
    // console.log('[UPDATE APP.js] Inside componentWillUpdate', nextProps, nextState);
  }

  // static getDerivedStateFromProps (nextProps, prevState) {
  //   console.log(
  //     '[UPDATE APP.js] Inside getDerivedStateFromProps', 
  //     nextProps, 
  //     prevState
  //     );

  //     return prevState;
  // }

  // getSnapshotBeforeUpdate () {
  //   console.log('getSnapShotBeforeUpdategetSnapShotBeforeUpdategetSnapShotBeforeUpdate')
  // }

  componentDidUpdate() {
    // console.log('[UPDATE APP.js] Inside componentDidUpdate');
  }
  // state = {
  //   persons : [
  //     {id: 'qwe', name: 'Max', age: 28},
  //     {id: 'asd', name: 'Manu', age: 29},
  //     {id: 'zxc', name: 'Stephanie', age: 26}
  //   ],
  //   otherState: 'some other value',
  //   showPersons: false
  // }

  nameChangedHandler = (event, id) => {
    const personIndex = this.state.persons.findIndex(p => {
      return p.id === id;
    })
    // creating copy of state as new JS object to avoid manipulation directly on state
    const person = {
      ...this.state.persons[personIndex]
    };
    // const person = Object.assign({}, this.state.persons[personIndex])
    
    // updating name of person on copy of state
    person.name = event.target.value;

    //updating array persons 
    const persons = [...this.state.persons];
    persons[personIndex] = person;

    //ste state with updated persons (persons is updated with exactly element)
    this.setState({persons: persons})
  }

  deletePersonHandler = (indexPerson) => {
    // kako je ovo prosledjena referenca do objekta, kada se doda slice na statet.persons
    // time se pravi KOPIJA niza, sto je pravilniji pristup nego samo da se radi SPLICE
    // druga opcija je spread operator
    // const persons = this.state.persons.slice();
    const persons = [...this.state.persons]
    persons.splice(indexPerson, 1);
    this.setState({persons: persons})
  }

  togglePersonsHandler = () => {
    const doesShow = this.state.showPersons;
    
    this.setState((prevState, props) => ({
      showPersons : !doesShow,
      toggleClicked: prevState.toggleClicked + 1
    }));



    // this.setState({showPersons : !doesShow});

    
  }

  loginHandler = () => {
    this.setState({authenticated: true});
  }

  render() {
    // console.log('[app.js] inside render()')
    let persons = null;

    if (this.state.showPersons) {
      persons = <Persons 
        persons = {this.state.persons}
        deletePersonHandler = {this.deletePersonHandler}
        nameChangedHandler = {this.nameChangedHandler}
        />
    }

    let cockpit = <Cockpit 
      appTitle ={this.props.title}
      persons = {this.state.persons}
      login = {this.loginHandler}
      showPersons = {this.state.showPersons}
      togglePersonsHandler = {this.togglePersonsHandler}/>
      
    return (
      <Ax>
      <button onClick={() => this.setState({showPersons:true})}>Show Persons</button>
        {cockpit}
        <AuthContext.Provider value={this.state.authenticated}>
          {persons}
        </AuthContext.Provider>
      </Ax>
    );

   // return React.createElement('div', {className:'App'}, React.createElement('h1', {className:'heading'}, 'neki tekst'))
  }
}

export default withClass( App, style.App );
