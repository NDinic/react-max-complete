import React, { PureComponent } from 'react'
import Person from './Person/Person'

class Persons extends PureComponent {
  constructor(props) {
    super(props);
    // console.log('[Persons.js] inside constructor', props)
    this.lastPersonRef = React.createRef();
  }

  componentWillMount() {
    // console.log('[Persons.js] inside componentWillMount()')
  }
  
  componentDidMount() {
    // console.log('[Persons.js] inside componentDidMount()')
    this.lastPersonRef.current.focus()
  }
  componentWillReceiveProps(nextProps) {
    // console.log('[UPDATE ONE person.js] Inside componentWillReceiveprops', nextProps);
  }

  // shouldComponentUpdate(nextProps, nextState) {
  //   console.log('[UPDATE ONE person.js] Inside shouldComponentUpdate', nextProps, nextState);
  //   return nextProps.person !== this.props.persons || 
  //   nextProps.changed !== this.props.changed || 
  //   nextProps.props.clicked !== this.props.clicked;
  //   // return true;
  // }

  componentWillUpdate(nextProps, nextState) {
    // console.log('[UPDATE ONE person.js] Inside componentWillUpdate', nextProps, nextState);
  }

  componentDidUpdate() {
    // console.log('[UPDATE ONE person.js] Inside componentDidUpdate');
  }
  render () {
    // console.log('[persons.js] inside persons render()')
    return this.props.persons.map((person, index) => {
        return <Person 
        click={this.props.deletePersonHandler.bind(this, index)}
          position = {index}
          name={person.name}
          age={person.age}
          ref={this.lastPersonRef}
          key={person.id}
          changed={(event) => this.props.nameChangedHandler(event, person.id)}
        />
      })
  }
}
export default Persons;



