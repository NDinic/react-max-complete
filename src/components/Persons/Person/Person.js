import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styles from './Person.module.css'
import withClass from '../../../hoc/withClass'
import Ax from '../../../hoc/Ax'
import { AuthContext } from '../../../containers/App' 


class Person extends Component {
  constructor(props) {
    super(props);
    //  console.log('[ONE Person.js] inside constructor', props)
     this.inputElement = React.createRef();
  }

  componentWillMount() {
    // console.log('[ONE Person.js] inside componentWillMount()')
  }
  
  componentDidMount() {
      // console.log('[ONE Person.js] inside componentDidMount()')
      
      if(this.props.position === 1) {
        this.inputElement.current.focus()
      }
      
  }

  focus() {
    this.inputElement.current.focus()
  }
  
  render () {
    // console.log('[ONE Person.js] inside componentDidMount()')
    return (
      <Ax>
        <AuthContext.Consumer>
          {auth => auth ? <p>Is Logged in</p> : null}
        </AuthContext.Consumer>
      <p onClick={this.props.click}>I am {this.props.name} and I am {this.props.age} years old </p>
      <p>{this.props.children}</p>
      <input 
        ref={this.inputElement}
        position = {this.props.index}
        type="text" 
        onChange={this.props.changed} 
        value={this.props.name}/>
    </Ax>
    )
  }
}

Person.propTypes = {
  click: PropTypes.func,
  name: PropTypes.string,
  age: PropTypes.number,
  changed: PropTypes.func
};

export default withClass(Person, styles.Person);