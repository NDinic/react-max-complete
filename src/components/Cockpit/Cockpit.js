import React from 'react'
import style from './Cockpit.module.css'
import Ax from '../../hoc/Ax'

const cockpit = (props) => {

  let btnStyle = style.CockpitButton;
  if (props.showPersons) {
    btnStyle = [style.CockpitButton, style.Red].join(' ');
  }
  const classes = [];
  if (props.persons.length <= 2) {
    classes.push(style.red);
  }
  if (props.persons.length <= 1) {
    classes.push(style.bold);
  }
  return (
    <Ax>
      <h1>{props.appTitle}</h1>
      <p className={classes.join(' ')}>This is really working!</p>
      <button
        className={btnStyle}
        onClick={props.togglePersonsHandler}>Toggle Persons</button>
        <button onClick={props.login}>Log in</button>
    </Ax>
  )
}

export default cockpit;